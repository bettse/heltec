# Heltec license RE

I noticed that the heltec license search (https://resource.heltec.cn/search) for similar ids resulted in similar licenses, indicating it isn't a cryptographic hash like sha1.  I wrote a script to collect values and try to visualize data to see the pattern.  I haven't cracked it, but I wanted to post what I did find.

### Notes
 * ChipID appears to be a reverse order MAC address (in most cases).  Correcting the order, you can find espressif results for the OUI.
 * It is possible the algorithm, in order to validate, is in the firmware of the lora modem.
