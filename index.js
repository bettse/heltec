const fetch = require('node-fetch');
const levenshtein = require('fast-levenshtein');
const { default: samples } = require('./samples');
const HTMLParser = require('node-html-parser');
const HEX = 0x10;

samples.sort(RASSort);

function reverseAndLowerId(sample) {
  const { id } = sample;
  const lower = id.toLowerCase().match(/(..?)/g);
  lower.reverse();
  return lower.join('');
}

function RASSort(a, b) {
  if (reverseAndLowerId(a) === reverseAndLowerId(b)) {
    return 0;
  }
  if (reverseAndLowerId(a) < reverseAndLowerId(b)) {
    return -1;
  }
  if (reverseAndLowerId(a) > reverseAndLowerId(b)) {
    return 1;
  }
}

function sequentialId(a, b) {
  if (a.id.toLowerCase() === b.id.toLowerCase()) {
    return 0;
  }
  if (a.id.toLowerCase() < b.id.toLowerCase()) {
    return -1;
  }
  if (a.id.toLowerCase() > b.id.toLowerCase()) {
    return 1;
  }
}

function levenshteinId(a, b) {
  return levenshtein.get(a.id, b.id);
}

function bitCount(x) {
  return x.toString(2).replace(/0/g,"").length
}

function bitCountId(a, b) {
  return bitCount(parseInt(a.id, HEX)) - bitCount(parseInt(b.id, HEX));
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

async function getLicense(id) {
  try {
    const params = new URLSearchParams();
    params.append('theId', id);

    const response = await fetch('https://resource.heltec.cn/search', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
      },
      body: params,
    });
    if (!response.ok) {
      throw new Error(`error getting ${id}`);
    }
    const body = await response.text();
    const root = HTMLParser.parse(body);

    //const id = root.querySelector('.result > tr:last-child td:nth-child(2)');
    const license = root.querySelector('.result > tr:last-child td:nth-child(3)')
    if (license) {
      return license.innerHTML;
    }
  } catch (e) {
    console.log('getLicense', e)
  }
  return '';
}

function xor(a, b) {
  const c = Buffer.alloc(a.length).fill(0);
  for(var i = 0; i < a.length; i++) {
    c[i] = a[i] ^ b[i];
  }
  return c
}

async function main() {
  //await sequential();
  //await offbyone();

  //return "";
  //var base = Buffer.alloc(0x10).fill(0);
  var base = Buffer.from('ab50c1632b6e96e2623041de6f5c1e45', 'hex');

  const subset = samples.filter(x => reverseAndLowerId(x).startsWith('246f28'))
  subset.forEach((sample, index) => {
    const { id, long } = sample;
    const reversedId = reverseAndLowerId(sample);

    if (index > 0) {
      const prev = reverseAndLowerId(subset[index - 1])
      const x = parseInt(reversedId.slice(-4), HEX)
      const y = parseInt(prev.slice(-4), HEX)
      const distance = (x - y) / 4;
      //const distance = levenshtein.get(reversedId, prev);
      for (var i = 0; i < distance-1; i++) {
        console.log("");
      }
    }

    const license = Buffer.from(long, 'hex')
    const x = xor(base, license);
    if (x.equals(Buffer.alloc(0x10).fill(0))) {
      //console.log('dup', id);
      //return;
    }

    console.log(reversedId, license.toString('hex'), Array.from(x).map(b => b.toString(2).padStart(8, '0')).join(' '))
    //console.log(id, Array.from(license).map(b => b.toString(2).padStart(8, '0')).join(' '))
    //console.log(reversedId, Array.from(license).map(b => b.toString(2).padStart(8, '0')).join('').replaceAll('0', ' ').replaceAll('1', 'X'))
    //base = license;
  })
  return "";
}

async function offbyone() {
  const ids = samples.map(sample => sample.id)
  ids.forEach(async id => {
    const bid = Buffer.from(id, 'hex')
    for(var i = 0; i < 6; i++) {
      if (i > 0) {
        bid[i - 1]++;
      }
      bid[i]--;
      const newId = bid.toString('hex');

      const license = await getLicense(newId);
      const long = license.split(',').map(p => p.replace('0x', '')).join('');
      if (long) {
        console.log({id: newId, long});
      }
    }
  })
}

async function sequential() {
  //const base = 0x0c33b528070b;
  var base = 0x00008d2de6b4;
  do {
    console.log({base: base.toString(HEX)})
    var found = 0
    var i = 0;
    do {
      const id = (base + i * 0x010000000000).toString(HEX).padStart(12, '0');

      const license = await getLicense(id);
      const long = license.split(',').map(p => p.replace('0x', '')).join('');
      if (long) {
        found++;
        console.log({id, long}, ',');
      }
      if(i > 0x20 && found < 2) {
        break;
      }
      i++; // could be +4.  In most cases the top most byte is 0/4/8/c
    } while (i < 0xff)
    base = base + 0x000100000000;
  } while (base < 0x00FF00000000);
}

main().then(console.log).catch(console.log)
